import React from 'react';
import styled, { withTheme } from 'styled-components';

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 25px 1fr;
  grid-column-gap: 10px;
`;

const Image = styled.img`
  display: block;
  width: 25px;
  height: 25px;
  border-radius: 50%;
  margin: 0;
  position: relative;
  top: 8px;
`;

const Name = styled.p`
  font-size: 0.75em;
  align-self: center;
`;

const NamePart = styled.span`
  display: inline-block;
`;

const CardInfoAuthor = (props) => {
  // user https://www.instagram.com/ ###user#### /
  return (
    <Wrapper className="CardInfoAuthor">
      <div><Image src={props.author.profile_image_url_https} alt="" onError={(e)=>{e.target.onerror = null; e.target.src="https://www.instagram.com/static/images/ico/favicon-192.png/68d99ba29cc8.png"}} /></div>
      <Name className="author">
        <NamePart>{props.author.name}</NamePart> -
        <NamePart>
            <a href={
                (((props.media[0].media_url_small).indexOf("instagram.com") === -1)?
                'https://twitter.com/'
                : 'https://www.instagram.com/') + props.author.screen_name} target="_blank" rel="noopener noreferrer">@{props.author.screen_name}</a>
        </NamePart>
      </Name>
    </Wrapper>
  );
}

export default withTheme(React.memo(CardInfoAuthor));
