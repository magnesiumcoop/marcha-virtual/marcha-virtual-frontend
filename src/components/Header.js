import React from 'react';
import styled, {withTheme} from 'styled-components';
import UsersCounter from './UsersCounter';

const Wrapper = styled.div`
  padding: 1em 15px;
  position: relative;
  @media (max-width: ${props => props.theme.pageWidth.m}px) {
    padding: 2em 0;
  }
`;

const Title = styled.h1`
  text-align: center;
  color: ${props => props.theme.colors.light};
  background-color: ${props => props.theme.colors.dark};
  font-family: ${props => props.theme.fonts.display};
  font-size: 2.7em;
  font-weight: 100; //Lighter como está en el pdf
  padding:.1em .5em;
  border-radius:.1em;
  margin: 0.25em 0;

  @media (max-width: ${props => props.theme.pageWidth.m}px) {
    font-size: 1.2em;
    margin-bottom: 0.5em;
  }
  
  @media (min-width: ${props => props.theme.pageWidth.m}px) {
    letter-spacing: 4px; //siguiendo el pdf
      font-size: 1.8em;
  }
  
  @media (min-width: ${props => props.theme.pageWidth.l}px) {
    font-size: 2em;
  }
  
  @media (min-width: ${props => props.theme.pageWidth.xl}px) {
    letter-spacing: 8px; //siguiendo el pdf
      font-size: 2.7em;
  }
`;

const SubTitle = styled.p`
  text-align: center;
  color: ${props => props.theme.colors.primary};
  font-family: ${props => props.theme.fonts.display};
  font-size: 1.00em;
  font-weight: 100;
  padding-top: 0.3em;
  //background-color:${props => props.theme.colors.primary};
  border-radius: .1em;
  margin: 0 auto 0.125em auto;


  @media (max-width: ${props => props.theme.pageWidth.m}px) {
    font-size: 0.8em;
    margin-bottom: 0.75em;
  }
`;

const Text = styled.p`
  text-align: center;  
  margin: 0;
  font-size: 1.52em;
  color: ${props => props.theme.colors.primary};
  
  @media (max-width: ${props => props.theme.pageWidth.m}px) {
    font-size: 0.8em;
  }

  span {
    display: inline-block;
  }
  
  a {
    color: #000;
    font-weight: 700;
  }
`;

const HeaderImage = styled.div`
  height: 100%;
  object-fit: contain;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-image: url("/familiares.png");
  
  @media (max-width: ${props => props.theme.pageWidth.m}px) {
      background-image: url("/margarita.png");
  }
`;

const Header = (props) => {

    return (
        <Wrapper className="Header">
            <div style={{display: "inline-flex"}}>
                <div style={{width: "25%", maxHeight: "190px"}}>
                    <HeaderImage/>
                </div>
                <div style={{width: "75%", padding:"10px"}}>
                    <Title>{props.title}</Title>
                    <Text>{props.children}</Text>
                    <SubTitle>{props.info}</SubTitle>
                    <UsersCounter count={props.count}></UsersCounter>
                </div>
            </div>
        </Wrapper>
    );
}

export default withTheme(React.memo(Header));