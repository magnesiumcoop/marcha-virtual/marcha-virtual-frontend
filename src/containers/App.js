import React, {Component} from 'react';
import {Route, withRouter} from 'react-router-dom';
import styled, {ThemeProvider} from 'styled-components';
import axios from 'axios';
import Api from '../api';

import Header from '../components/Header';
import Media from '../components/Media';
import Card from '../components/Card';
import Login from '../components/Login';
import Constants from '../constants';

const theme = {
    colors: {
        dark: '#04090d',
        light: '#f8f8f8',
        primary: '#243243'
    },
    fonts: {
        display: "'Roboto', 'sans-serif'",
        text: "'Roboto Mono', 'sans-serif'"
    },
    pageWidth: {
        xl: 1200,
        l: 992,
        m: 768,
        s: 576
    },
    columns: {
        xl: 24,
        l: 19,
        m: 12,
        s: 9,
        gap: {
            xl: 5,
            l: 5,
            m: 5,
            s: 5
        }
    }
};

const finalDate = new Date("May 20, 2021")

const Container = styled.div`
  position: relative;
  margin: 0 auto;
  padding: 0 30px;
  width: 100%;
  @media (min-width: ${theme.pageWidth.s}px) {
    max-width: ${theme.pageWidth.s}px;
  }
  @media (min-width: ${theme.pageWidth.m}px) {
    max-width: ${theme.pageWidth.m}px;
  }
  @media (min-width: ${theme.pageWidth.l}px) {
    max-width: ${theme.pageWidth.l}px;
  }
  @media (min-width: ${theme.pageWidth.xl}px) {
    max-width: ${theme.pageWidth.xl}px;
  }
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: ${'----------------------------------------'
    .substring(0, theme.columns.s)
    .replace(/-/gi, '1fr ')};
  grid-column-gap: ${theme.columns.gap.s}px;
  grid-row-gap: ${2 * theme.columns.gap.s}px;
  margin: 60px 0;
  transform: rotate3d(0deg, 0deg, 0deg);

  @media (min-width: ${theme.pageWidth.s}px) {
    grid-template-columns: ${'----------------------------------------'
    .substring(0, theme.columns.s)
    .replace(/-/gi, '1fr ')};
    grid-column-gap: ${theme.columns.gap.s}px;
    grid-row-gap: ${2 * theme.columns.gap.s}px;
  }
  @media (min-width: ${theme.pageWidth.m}px) {
    grid-template-columns: ${'----------------------------------------'
    .substring(0, theme.columns.m)
    .replace(/-/gi, '1fr ')};
    grid-column-gap: ${theme.columns.gap.m}px;
    grid-row-gap: ${2 * theme.columns.gap.m}px;
  }
  @media (min-width: ${theme.pageWidth.l}px) {
    grid-template-columns: ${'----------------------------------------'
    .substring(0, theme.columns.l)
    .replace(/-/gi, '1fr ')};
    grid-column-gap: ${theme.columns.gap.l}px;
    grid-row-gap: ${2 * theme.columns.gap.l}px;
  }
  @media (min-width: ${theme.pageWidth.xl}px) {
    grid-template-columns: ${'----------------------------------------'
    .substring(0, theme.columns.xl)
    .replace(/-/gi, '1fr ')};
    grid-column-gap: ${theme.columns.gap.xl}px;
    grid-row-gap: ${2 * theme.columns.gap.xl}px;
  }
`;

const HeaderWrapper = styled.header`
  grid-column: 1 / span ${theme.columns.s};
  grid-row: 3 / span 4;
  justify-self: center;
  align-self: center;

  @media (min-width: ${theme.pageWidth.s}px) {
    grid-column: 1 / span ${theme.columns.s};
  }
  @media (min-width: ${theme.pageWidth.m}px) {
    grid-column: 2 / span ${theme.columns.m - 2};
  }
  @media (min-width: ${theme.pageWidth.l}px) {
    grid-column: 3 / span ${theme.columns.l - 4};
  }
  @media (min-width: ${theme.pageWidth.xl}px) {
    grid-column: 3 / span ${theme.columns.xl - 4};
  }
`;


const Footer = styled.footer`
  position: fixed;
  bottom: 0;
  right: 0;
  padding: 0.25em 30px 0.25em;
  background: rgba(248, 248, 248, 0);
  background: -moz-linear-gradient(
    left,
    rgba(248, 248, 248, 0) 100%,
    rgba(248, 248, 248, 0.95) 25%,
    rgba(248, 248, 248, 1) 0%
  );
  background: -webkit-gradient(
    left top,
    right top,
    color-stop(0%, rgba(248, 248, 248, 0)),
    color-stop(25%, rgba(248, 248, 248, 0.95)),
    color-stop(100%, rgba(248, 248, 248, 1))
  );
  background: -webkit-linear-gradient(
    left,
    rgba(248, 248, 248, 0) 0%,
    rgba(248, 248, 248, 0.95) 25%,
    rgba(248, 248, 248, 1) 100%
  );
  background: -o-linear-gradient(
    left,
    rgba(248, 248, 248, 0) 0%,
    rgba(248, 248, 248, 0.95) 25%,
    rgba(248, 248, 248, 1) 100%
  );
  background: -ms-linear-gradient(
    left,
    rgba(248, 248, 248, 0) 0%,
    rgba(248, 248, 248, 0.95) 25%,
    rgba(248, 248, 248, 1) 100%
  );
  background: linear-gradient(
    to right,
    rgba(248, 248, 248, 0) 0%,
    rgba(248, 248, 248, 0.95) 25%,
    rgba(248, 248, 248, 1) 100%
  );
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f8f8f8', endColorstr='#f8f8f8', GradientType=1 );
  text-align: right;
`;

const Link = styled.a`
  color: inherit;
  font-family: ${theme.fonts.display};
  text-decoration: none;
  font-size: 0.625rem;
  display: block;
`;

const Modal = styled.div`
  position: absolute;
  z-index: 2;
  animation: 'in 400ms ease-out';
`;

const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.3);
  z-index: 2;
  animation: in 500ms ease-in-out;
`;

const Countdown = styled.div`
  font-family: ${props => props.theme.fonts.display};
  text-align: center;
  padding: 3em;
`;

const Consigna = styled.div`
  font-family: ${props => props.theme.fonts.display};
  padding: 2em;
  color: ${props => props.theme.colors.primary};
  font-family: ${props => props.theme.fonts.display};
  font-size: 1.00em;
  font-weight: 100;
  @media (max-width: ${props => props.theme.pageWidth.m}px) {
    font-size: 0.8em;
    margin-bottom: 0.75em;
  }
`;

const Preloader = styled.div`
  text-align: center;
  padding: 3em;
`;

const DummyImage = styled.div`
  height: 100%;
  object-fit: contain;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: 50% 50%;
  background-image: url("/invitacion2.jpeg");
  
  @media (max-width: ${props => props.theme.pageWidth.m}px) {
      background-image: url("/invitacion2.jpeg");
  }
`;

const {REACT_APP_API_URL: API_URL} = process.env;

class FeedComponent extends Component {
    state = {
        loading: false,
        tweets: [],
        currentTweet: null,
        currentPage: 1,
        perPage: Constants.initialAmount,
        total: 0,
        isAuthenticated: false,
        usersCount: 0,
        showEmpty: Constants.dummy_grid,
        date: {
            days: 0,
            hours: 0,
            min: 0,
            sec: 0,
        }
    };

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
        document.addEventListener('keydown', this.keyPressed);
        this.container = React.createRef();
        this.timer = null;
        const {currentPage: _currentPage, perPage} = this.state;
        const endpoint = 'tweets';
        const params = `page=${_currentPage}&perPage=${perPage}`;
        const url = `${API_URL}/${endpoint}?${params}`;
        this.fetchTweets(url);
        this.fetchUsersCount();
        //Countdown
        // update every second
        this.interval = setInterval(() => {
            const date = this.calculateCountdown(finalDate);
            if (date) {
                this.setState({date})
            } else {
                this.stop();
            }
        }, 1000);

    }

    componentWillReceiveProps(nextProps) {
        const {
            location: {state: {isAuthenticated = false} = {}}
        } = this.props.history;
        this.setState({isAuthenticated});
    }

    fetchTweets(url) {
        if (this.state.showEmpty !== "true" && !this.state.loading) {
            const {currentPage: _currentPage, tweets: _tweets} = this.state;
            this.setState({loading: true});
            axios.get(url).then(res => {
                const {list: newTweets, total} = res.data;
                const currentPage = _currentPage + 1;
                const tweets = new Set(_tweets.concat(newTweets));

                this.setState({
                    tweets: Array.from(tweets),
                    currentPage,
                    total,
                    loading: false
                });
            });
        }
    }

    fetchUsersCount = () => {
        Api.users.usersCount().then(res => {
            const {status} = res;
            if (status === 200) {
                this.setState({
                    usersCount: res.data.count
                });
            }
        });
    };

    onEndReached() {
        const {perPage, tweets} = this.state;
        const endpoint = 'tweets';
        const _perPage = Constants.perPage;
        const page = Math.round(tweets.length / _perPage) + 1;
        const params = `page=${page}&perPage=${perPage}`;
        const url = `${API_URL}/${endpoint}?${params}`;

        this.setState({currentPage: page, perPage: _perPage});
        this.fetchTweets(url);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
        //countdown
        this.stop();
    }

    calculateCountdown(endDate) {
        let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000;

        // clear countdown when date is reached
        if (diff <= 0) return false;

        const timeLeft = {
            years: 0,
            days: 0,
            hours: 0,
            min: 0,
            sec: 0,
            millisec: 0,
        };

        // calculate time difference between now and expected date
        if (diff >= (365.25 * 86400)) { // 365.25 * 24 * 60 * 60
            timeLeft.years = Math.floor(diff / (365.25 * 86400));
            diff -= timeLeft.years * 365.25 * 86400;
        }
        if (diff >= 86400) { // 24 * 60 * 60
            timeLeft.days = Math.floor(diff / 86400);
            diff -= timeLeft.days * 86400;
        }
        if (diff >= 3600) { // 60 * 60
            timeLeft.hours = Math.floor(diff / 3600);
            diff -= timeLeft.hours * 3600;
        }
        if (diff >= 60) {
            timeLeft.min = Math.floor(diff / 60);
            diff -= timeLeft.min * 60;
        }
        timeLeft.sec = diff;

        return timeLeft;
    }

    stop() {
        clearInterval(this.interval);
    }

    addLeadingZeros(value) {
        value = String(value);
        while (value.length < 2) {
            value = '0' + value;
        }
        return value;
    }

    isBottom(element) {
        return (
            document.getElementById('root').getBoundingClientRect().bottom <=
            window.innerHeight
        );
    }

    handleScroll = e => {
        const {total, tweets} = this.state;
        const shouldFetchMore = total > tweets.length;
        if (shouldFetchMore && this.isBottom()) {
            document.removeEventListener('scroll', this.trackScrolling);
            this.onEndReached();
        }
    };

    mouseClickHandler = (e, tweet) => {
        clearTimeout(this.timer);
        let ct = {tweet, el: e.target};
        this.setState({currentTweet: ct});
    };

    mouseEnterHandler = (e, tweet) => {
        let ct = {tweet, el: e.target};
        this.timer = setTimeout(() => {
            this.setState({currentTweet: ct});
        }, 800);
    };

    mouseLeaveHandler = () => {
        clearTimeout(this.timer);
    };

    closeCard = () => {
        this.setState({currentTweet: null});
    };

    deleteTweet = tweetId => {
        Api.users.deleteTweet(tweetId).then(res => {
            const {status} = res;
            if (status === 200) {
                console.log('res', res);
                console.log(`Deleted tweet with id: ${tweetId}`);
            }
        });
    };

    banUser = userTwitterId => {
        Api.users.banUser(userTwitterId).then(res => {
            const {status} = res;
            if (status === 201) {
                const {
                    data: {
                        inserted: {user_id_str},
                        removedTweetsCount
                    }
                } = res;
                console.log('res', res);
                console.log(`Banned user with twitter id: ${user_id_str}`);
                console.log(`Deleted ${removedTweetsCount} tweets`);
            }
        });
    };

    keyPressed = (e) => {
        if (e.keyCode === 77 && e.shiftKey) {
            this.props.history.push('/moderar');
        }
    }

    render() {
        const {isAuthenticated, usersCount} = this.state;

        let gallery = this.state.tweets.map(tweet => {
            return (
                <Media
                    key={tweet.tweet_id_str}
                    tweet={tweet}
                    alt=""
                    click={this.mouseClickHandler}
                    enter={this.mouseEnterHandler}
                    leave={this.mouseLeaveHandler}
                />
            );
        });

        let tweetCard = null;
        if (this.state.currentTweet) {
            let containerRect = this.container.current.getBoundingClientRect();
            let elemRect = this.state.currentTweet.el.getBoundingClientRect();
            let x =
                elemRect.left -
                containerRect.left +
                (elemRect.right - elemRect.left) / 2 -
                180;
            let y = elemRect.top - containerRect.top - 30;

            if (x + 360 > containerRect.right - containerRect.left + 15)
                x = containerRect.right - containerRect.left + 15 - 360;
            if (x < 15) x = 15;
            if (y < -25) y = -25;

            tweetCard = (
                <Modal style={{top: y, left: x}}>
                    <Overlay onTouchStart={this.closeCard}/>
                    <Card
                        show={isAuthenticated}
                        tweet={this.state.currentTweet.tweet}
                        close={this.closeCard}
                        delete={this.deleteTweet}
                        block={this.banUser}
                    />
                </Modal>
            );
        }

        let preloader = this.state.loading ? (
            <Preloader>
                <img src={require('../assets/spinner.gif')} alt="Cargando"/>
            </Preloader>
        ) : null;

        return (
            <Container ref={this.container} className="App">
                <Grid>
                    <HeaderWrapper>
                        {/*<Image src='/24.png' alt='' width="360" height="230" />*/}

                        <Header
                            title="#MarchaDelSilencio2021"
                            info="El 20 de mayo marchamos desde nuestros hogares. Sumate en tus redes con #MarchadelSilencio2021 #MarchadelSilencioPresente #MayoMesdelaMemoria"
                            count={usersCount}
                        >

                            <strong>¿Dónde están? No al silencio ni a la impunidad <br/> Memoria, Verdad y Justicia</strong>


                        </Header>
                        {this.state.showEmpty === "true" &&
                        <div>

                            <Consigna>
                                <p>Faltan <strong>{this.addLeadingZeros(this.state.date.days)}</strong>
                                    <span> {this.state.date.days === 1 ? 'día' : 'días'}</span> y <strong>{this.addLeadingZeros(this.state.date.hours)}</strong> horas
                                    para el <strong>20 de Mayo</strong>. 
                                    <p>Podés subir todas las fotos y videos que hagas con tus intervenciones a tus
                                        redes sociales usando <strong>#MarchaDelSilencioPresente</strong>, <strong>#MarchadelSilencio2021</strong> y <strong>#MayoMesdelaMemoria</strong>.

                                        De esta forma, el 20 de Mayo a través de una marcha virtual en <a href={"https://marchadelsilencio.uy/"} target={"_blank"}>www.marchadelsilencio.uy</a>,
                                         estaremos todos juntos diciendo PRESENTE.</p>



                                    <ul>

                                        <li><strong>Familiares de Detenidos Desaparecidos:</strong>
                                            <span><a
                                                href={"https://es-la.facebook.com/Madres-y-Familiares-de-Uruguayos-Detenidos-Desaparecidos-Famidesa-260635920937369/"}
                                                target={"_blank"}> Facebook</a></span> |
                                            <span><a href={"https://twitter.com/famidesa"}> Twitter</a></span> |
                                            <span><a
                                                href={"https://www.instagram.com/madresyfamiliares_uy"}> Instagram</a></span> |
                                            <span><a href={"https://desaparecidos.org.uy/"}> Web</a></span>

                                        </li>
                                        <li><strong>Vivos por la Memoria:</strong>
                                        <span><a href={"https://www.vivosennuestramemoria.com/"}> Web</a></span>


                                        </li>


                                    </ul>

                                </p>
                            </Consigna>
                        </div>
                        }
                    </HeaderWrapper>
                    {gallery}
                </Grid>
                {preloader}
                {tweetCard}
                <Footer>
                    {/* <img src='/favicon.png' width='48' alt='Marcha Virtual' /> */}
                    <Link
                        href="https://facttic.org.ar/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Desarrollado por FACTTIC
                    </Link>
                    <Link
                        href="https://magnesium.coop/"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Adaptado por Magnesium.coop
                    </Link>
                </Footer>
            </Container>
        );
    }
}

const Feed = withRouter(FeedComponent);

export default () => {
    return (
        <ThemeProvider theme={theme}>
            <Route path="/">
                <Feed/>
            </Route>
            <Route path="/moderar">
                <Overlay/>
                <Login/>
            </Route>
        </ThemeProvider>
    );
};
